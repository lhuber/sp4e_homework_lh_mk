#!/usr/bin/env python
from setuptools import setup

setup(name='optimization',
      version='0.1',
      description='Optimizer Library for sp4e',
      packages=[
          'optimization',
          ],
      scripts=[
          'scripts/main_optimizer.py',
          ],
      package_dir={'': 'src'}
     )
