"""
Generalized Minimal Residual Method

This exercise was part of course
'Scientific Programming For Engineers' at EPFL - Autumn 2021
"""
# Author: Lukas Huber, Mikhail Koptev
# Email: lukas.huber@epfl.ch mikhail.koptev@epfl.ch
# Created: 2021-10-14

import numpy as np

from optimization.einsum_utils import einsum_norm, einsum_matmul


def arnoldi(A: np.ndarray, Q: np.ndarray, k: int):
    """ Returns one iterative step of the optimization problem.
    
    Arguments
    ---------
    A: 2D matrix of the optimization problem
    Q: Step matrix
    k: Iteration dimensions
    
    Returns
    -------
    h [np.ndarray]: Iterated h value
    q [np.ndarray]: Iterated q value
    """
    h = np.zeros(k+2)
    q = einsum_matmul(Q[:,k],A.T)
    for i in range(k+1):
        h[i] = einsum_matmul(q, Q[:,i])
        q = q - h[i] * Q[:, i]
        
    h[k+1] = einsum_norm(q)
    q = q / h[k+1]
    
    return h, q 

def gmres_solve(A: np.ndarray, b: np.ndarray, x0: np.ndarray, max_iter: int, atol: float):
    """ Return GMRES solver for optimization problem

    Arguments
    ---------
    A: 2D (square matrix) of the optimization problem
    b: 1D matrix of the optimization problem
    x0: Starting position (1D)
    max_iter: Maximum number of itartions of the search
    atol: Absolute error tolerance at which search will stop.

    Returns
    -------
    x: optimal value (np.ndarray)
    exitCode [bool]: 0 -> success 
    x_traj: An 2D np.ndarray containing the trajectory of all points during the optimization 
    err_vec: A 1D np.ndarray containing all the error values along the trajectory
    """
    n = len(A)
    m = max_iter
    x = x0
    r = b - einsum_matmul(A, x)
    # r = b - np.dot(A, x)

    r_norm = einsum_norm(r)
    
    e1 = np.zeros(m + 1)
    e1[0] = 1.0
    
    # Beta is the beta vector instead of the beta scalar
    beta = r_norm * e1 
    
    H = np.zeros((m+1, m+1))
    err_vec = []
    Q = np.zeros((n, m+1))
    Q[:,0] = r / r_norm
    x_traj = []

    x_cur = x
    for k in range(m):
        x_old = x_cur
        (H[0:k+2, k], Q[:, k+1]) = arnoldi(A, Q, k)
        
        # We use np.linalg.lstsq to find y
        y = np.linalg.lstsq(H[0:m+1, 0:m], beta, rcond=None)[0]
        
        x_cur = x + einsum_matmul((Q[:,0:k+1]).T, y[0:k+1])
        err_cur = einsum_matmul(A,x_cur)-b
        x_traj.append(x_cur)
        err_vec.append(err_cur)
        diff = einsum_norm(x_cur-x_old)

        if diff < atol:
            x = x + einsum_matmul((Q[:,0:k+1]).T, y[0:k+1])
            return(x, 0, x_traj, err_vec)
    
    return(x, 1, x_traj, err_vec)
