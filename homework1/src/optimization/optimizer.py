"""
Minimal Residual Method

This exercise was part of course
'Scientific Programming For Engineers' at EPFL - Autumn 2021
"""
# Author: Lukas Huber, Mikhail Koptev
# Email: lukas.huber@epfl.ch mikhail.koptev@epfl.ch
# Created: 2021-10-14

from abc import ABC, abstractmethod
from enum import Enum

import numpy as np
from scipy.optimize import minimize
from scipy.sparse.linalg import lgmres

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from optimization.GMRES import gmres_solve


class OptimizationType(Enum):
    """ Define the different optimization methods."""
    MINIMIZE = 0
    LGMRES = 1
    GMRES_CUSTOM = 2
    

class OptimizationFunctor(ABC):
    """ Virtual Optimizer Functor. """
    @abstractmethod
    def __call__(self, x: np.array) -> np.array:
        pass
    
class QuadraticFunctor(OptimizationFunctor):
    dimension = 2
    
    def __init__(self, A: np.array, b: np.array):
        self.A = A
        self.b = b

    def __call__(self, x: np.array) -> np.array:
        return 0.5 * x.T @ self.A @ x - x @ self.b

    def get_initial_guess(self) -> np.array:
        return self.b
    

class MainOptimizer():
    a_tol_conv = 1e-5
    dimension = 2
    def __init__(self):
        self._x_traj = np.zeros((0, self.dimension))

    def optimize(
        self, optimization_funct:OptimizationFunctor,
        method_type: OptimizationType = OptimizationType.MINIMIZE) -> np.array:

        # Initial Guess
        x0 = optimization_funct.get_initial_guess()
        
        if method_type == method_type.MINIMIZE:
            opt_result = minimize(optimization_funct, x0, tol=self.a_tol_conv)

            if not opt_result.success:
                return None

            x_min = opt_result.x

        elif method_type == method_type.LGMRES:
            x_min, exitCode = lgmres(
                optimization_funct.A, optimization_funct.b,
                x0=x0,
                atol=self.a_tol_conv)
            
        elif method_type == method_type.GMRES_CUSTOM:
            (x_min, exitCode, x_traj, err_vec) = gmres_solve(
                optimization_funct.A, optimization_funct.b,
                x0=x0,
                max_iter=1000,
                atol=self.a_tol_conv)

            if exitCode != 0:
                return None
        
        else:
            raise Exception(f"Not implemeted for type: {method_type}")

        return x_min
    
    def visualize_steps(
        self, optimization_funct:OptimizationFunctor,
        method_type: OptimizationType = OptimizationType.MINIMIZE,
        x0=None,
        x_lim=[-3.5, 3.5], y_lim=[-3.5, 3.5], n_grid=40) -> None:

        x_vals, y_vals = np.meshgrid(np.linspace(x_lim[0], x_lim[1], n_grid),
                                     np.linspace(y_lim[0], y_lim[1], n_grid))

        positions = np.vstack((x_vals.reshape(1, -1), y_vals.reshape(1, -1)))
        z_values = np.zeros(positions.shape[1])

        for ii in range(positions.shape[1]):
            z_values[ii] = optimization_funct(positions[:, ii])

        if x0 is None:
            x0 = optimization_funct.get_initial_guess()

        fig = plt.figure(figsize=(14, 6))
        
        # Evaluate with minimize
        ax = fig.add_subplot(1, 3, 1, projection='3d')
        ax.set_title("BFGS")

        self.reset_callback(x0)
        opt_result = minimize(optimization_funct, x0,
                              tol=self.a_tol_conv, callback=self.callback_storer)
        z_traj = [optimization_funct(self._x_traj[ii, :])
                  for ii in range(self._x_traj.shape[0])]
        
        ax.plot_surface(x_vals, y_vals, z_values.reshape(n_grid, n_grid),
                        cmap=plt.get_cmap('viridis'), alpha=0.8,
                        )
        ax.plot(self._x_traj[:, 0], self._x_traj[:, 1], z_traj, 'r--', marker=".")
        
        # Repeat for GMRES
        ax = fig.add_subplot(1, 3, 2, projection='3d')
        ax.set_title("GMRES")
        
        # ax.contourf3D(x_vals, y_vals, z_values.reshape(n_grid, n_grid),
                      # cmap=plt.get_cmap('viridis'), alpha=0.8,
                      # )

        self.reset_callback(x0)
        x_min, exitCode = lgmres(
                optimization_funct.A, optimization_funct.b, atol=self.a_tol_conv,
                x0=x0,
                callback=self.callback_storer)
        
        z_traj = [optimization_funct(self._x_traj[ii, :])
                  for ii in range(self._x_traj.shape[0])]
        
        ax.plot_surface(x_vals, y_vals, z_values.reshape(n_grid, n_grid),
                        cmap=plt.get_cmap('viridis'), alpha=0.8,
                        )
        ax.plot(self._x_traj[:, 0], self._x_traj[:, 1], z_traj, 'r--', marker=".")

        # Repeat for GMRES_custom
        ax = fig.add_subplot(1, 3, 3, projection='3d')
        ax.set_title("GMRES_custom")

        (x_min, exitCode, x_traj, err_vec) = gmres_solve(
                optimization_funct.A, optimization_funct.b,
                x0=x0,
                max_iter=1000,
                atol=self.a_tol_conv)
        self._x_traj = np.array(x_traj)
        z_traj = [optimization_funct(self._x_traj[ii, :])
                  for ii in range(self._x_traj.shape[0])]
        
        ax.plot_surface(x_vals, y_vals, z_values.reshape(n_grid, n_grid),
                        cmap=plt.get_cmap('viridis'), alpha=0.8,
                        )
        ax.plot(self._x_traj[:, 0], self._x_traj[:, 1], z_traj, 'r--', marker=".")

    def reset_callback(self, x0: np.ndarray=None):
        """ Reset the _x_traj variable before starting the callback. """
        print("") 
        self._x_traj = np.zeros((0, self.dimension))

        if x0 is not None:
            self._x_traj = np.vstack((self._x_traj, x0))
        
    def callback_storer(self, xk: np.ndarray) -> None:
        """ Store iteartive position to _x_traj property. """
        print(f"ii: {self._x_traj.shape[0]} --- x: {xk}")
        self._x_traj = np.vstack((self._x_traj, xk))
