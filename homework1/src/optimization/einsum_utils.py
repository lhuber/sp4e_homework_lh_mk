"""
Created on Fri Oct 15 12:26:43 2021

@author: michael, lukas
"""
import numpy as np


def einsum_norm(a):
    """Returns the norm of the input vector using the einsum-matrix multipicplation function. """
    return(np.sqrt(einsum_matmul(a,a)))

def einsum_matmul(a,b):
    """Returns the vector/matrix multiplication using the einsum method. """
    dimA = a.ndim
    dimB = b.ndim
    if dimA == 1 and dimB == 2:
        return(np.einsum('i,ij->j', a, b))
    elif dimA == 2 and dimB == 1:
        return(np.einsum('ij,i->j', a, b))
    elif dimA == dimB == 1:
        return(np.einsum('i,i', a, b))
    elif dimA == dimB == 2:
        return(np.einsum('ij,jk', a, b))
    else:
        raise Exception(f"Unexpected input dimensions A={A.shape} and B={B.shape}.")
