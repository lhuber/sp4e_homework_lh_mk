#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Main Parser-File

Description on how to use it can be found in the README.md

This exercise was part of course
'Scientific Programming For Engineers' at EPFL - Autumn 2021
"""
# Author: Lukas Huber, Mikhail Koptev
# Email: lukas.huber@epfl.ch mikhail.koptev@epfl.ch
# Created: 2021-10-18

import argparse

import numpy as np
import matplotlib.pyplot as plt

from optimization.optimizer import QuadraticFunctor, MainOptimizer
from optimization.optimizer import OptimizationType


if (__name__) == "__main__":
    my_parser = argparse.ArgumentParser(description='Process input.')
    
    my_parser.add_argument('A', metavar='A',
                           help='System Matrix A')

    my_parser.add_argument('b', metavar='b',
                           help='System vector b')

    my_parser.add_argument('-p',
                           '--plot',
                           action='store_true',
                           help='Plot or not!')
    
    # Optimization types -> default is minimze (only considered when no plotting)
    my_parser.add_argument('-m',
                           '--minimize',
                           action='store_true',
                           help='Optimization Type: minimize')

    my_parser.add_argument('-l',
                           '--lgmres',
                           action='store_true',
                           help='Optimization Type: lgmres')

    my_parser.add_argument('-g',
                           '--gmres-custom',
                           action='store_true',
                           help='Optimization Type: gmres custom')

    args = my_parser.parse_args()

    A = np.array(eval(args.A))
    b = np.array(eval(args.b))

    if len(A.shape)!=2 or A.shape[0] != A.shape[1]:
        raise Exception("A-matrix needs to be a square matrix.")

    if A.shape[0] != b.shape[0]:
        raise Exception("A-matrix and b-matrix do not have the same dimensions.")

    my_optimization = QuadraticFunctor(A, b)
    MyOptimizer = MainOptimizer()

    # Plot or not
    if args.plot:
        plt.close('all')
        visualize_steps = MyOptimizer.visualize_steps(
            optimization_funct=my_optimization)

        plt.show()
        
    else:
        if args.minimize:
            opt_method = OptimizationType.MINIMIZE
        elif args.lgmres:
            opt_method = OptimizationType.LGMRES
        elif args.gmres_custom:
            opt_method = OptimizationType.GMRES_CUSTOM
        else:
            # Default method
            opt_method = OptimizationType.MINIMIZE

        opt_value = MyOptimizer.optimize(optimization_funct=my_optimization,
                                               method_type=opt_method)
        print(f"Optimal value is: {opt_value}.")
