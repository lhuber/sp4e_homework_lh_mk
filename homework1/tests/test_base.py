"""
Testing file

This exercise was part of course
'Scientific Programming For Engineers' at EPFL - Autumn 2021
"""
# Author: Lukas Huber, Mikhail Koptev
# Email: lukas.huber@epfl.ch mikhail.koptev@epfl.ch
# Created: 2021-10-14

import numpy as np

from optimization.optimizer import QuadraticFunctor, MainOptimizer
from optimization.optimizer import OptimizationType
from optimization.GMRES import gmres_solve

def test_custom_solver():
    # Functional test only
    A = np.array([[8, 1], [1, 3]], dtype=float)
    b = np.array([2, 4], dtype=float)
    x0 = np.array([.0, .0], dtype=float)
    (x, exit_code, x_traj, err_vec) = gmres_solve(A, b, x0, 200, 1e-5)
    print(x_traj)

    # assert

def test_different_sovlers():
    # Specify optimization function params
    A = np.array([[8, 1], [1, 3]], dtype=float)
    b = np.array([2, 4], dtype=float)
    
    #or, alternatively, use random matrices of selected dimension
    # dim = 5
    # np.random.seed(0)
    # A = np.random.rand(dim,dim)
    # b = np.random.rand(dim)

    #create functor and optimizer entities
    my_optimization = QuadraticFunctor(A,b)
    MyOptimizer = MainOptimizer()
    
    #call minimization with scipy.optimize.minimize routine
    x_min = MyOptimizer.optimize(
        optimization_funct=my_optimization, 
        method_type=OptimizationType.MINIMIZE)
    print('Minimum - minimze: ', x_min)
    
    #call minimization with scipy.sparse.linalg.lgmres routine
    x_min = MyOptimizer.optimize(
        optimization_funct=my_optimization,
        method_type=OptimizationType.LGMRES)
    print('Minimum - lgmres: ', x_min)
    
    #call minimization with custom gmres routine
    x_min = MyOptimizer.optimize(
        optimization_funct=my_optimization,
        method_type=OptimizationType.GMRES_CUSTOM)
    print('Minimum - Custom gmres: ', x_min)

    # assert
