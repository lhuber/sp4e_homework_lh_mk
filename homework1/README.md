# Getting Started

## Installing and activate new environment with conda (optional)
Alternatively use your preferred virtual (python) environment.
```
conda env create -f environment.yml
```

Activate the conda environment.
```
conda activate sp4e
```

## Installing python libraries
```
pip install -r requirements.txt
```

Setup environment
```
python setup.py develop
```

## Optimize with different values
Usage in (bash) console:
```
main_optimizer.py [-h] [-p] [-m] [-l] [-g] A b
```

Inputs have to be of the form: <br/>
`A` matrix <br/>
`b` vector <br/>

Method type can be: <br/>
`-m, --minimze` for optimization minimization <br/>
`-l, --lgmres `  for Sparse LGMRES <br/>
` -g, --gmres-custom` for Custom implemented GMRES  <br/>
`-p, --plot` to visualize the different method use the argument  <br/>
	
Example to find minimum with the LGMRES method:
```
main_optimizer.py [[8,1],[1,3]] [2,4] -l
```

Example to compare the different methods:
``` bash
main_optimizer.py [[8,1],[1,3]] [2,4] -p
```

## Run tests (developers only)
Go to home-folder, and run:
```
pytest
```
