# Homework 2 [Week 6]
Authors : Lukas Huber & Mikhail Koptev <br/>
Email: lukas.huber@epfl.ch, mikhail.koptev@epfl.ch <br/>

## Set-Up & Run
### Initialize 
Move to the build directory:
```bash
cd build
```

### Create Project
In the `build` directory, type:
```bash
cmake ..
```

### Build 
In the `build` directory, type:
```bash
make
```

### Run
Execute from the `build` directory.

The program has following structure:
```bash
./src/main -n [maxiter] -f [frequency]  [seriestype + (arguments)] [print/write + (delimiter)] 

If you wish to calculate series specify whether you wish to compute Pi (--pi) or Arithmetic (--arithmetic) 
If you want to calculate integral, then please specify (in order!): 
--cos, --sin, or --cube : function type, must be followed by two arguments below 
DOUBLE : interval start 
DOUBLE : interval end  

Shared parameters for series/integrals 
-n INT : number of iterations/partitions 
-f INT : output frequency for Dumper class 
--print : output to terminal 
--write STRING : output to file followed by the one of the keywords [comma, pipe, space] 
```

Compute PI with 1000 iterations and output result to .csv file with frequency of 10
``` bash
./src/main --pi -n 1000 -f 10 --write comma
```

Compute arithmetic series with 300 iterations and output result a `*.psv` file with frequency of 20
``` bash
./src/main --arithmetic -n 300 -f 20 --write pipe
```

Compute arithmetic series with 300 iterations and output result a `*.txt` file with frequency of 20
``` bash
./src/main --arithmetic -n 300 -f 20 --write space
```

Compute integral_0^1(x^3dx) with 100 iterations
``` bash
./src/main -n 100 -f 1 --cube 0 1
``` 

Compute integral_0^pi(cos(x)dx) with 100 iterations
``` bash
./src/main --print -n 100 -f 1 --cos 0 3.1345
```

Compute integral_0^pi/2(sin(x)dx) with 100 iterations
``` bash
./src/main --print -n 100 -f 1 --sin 0 1.5707
```

### Plot Data Using Python
Make sure you have `matplotlib`:
``` bash
pip install matplotlib
```

In the `src` folder run:
``` bash
python plot_series.py [filename]
```

I.e. for the three examples run: <br/>
Text file:
``` bash
python plot_series.py output.txt
```
Csv-file
``` bash
python plot_series.py output.csv
```
Psv-file
``` bash
python plot_series.py output.psv
```

## Answers to Questions
All files are contained in the `src`-folder. We used one-file per family. This choice was as a result of small filesize (< 200 lines per file) and the feedback from the last homework, that we used too many files.

### Strategies to Divide Work
In general, work can be divided well among people if different features, tests etc. are implemented. Each person can work on a specific part, without bothering the others. 

For small projects, dividing the work becomes difficult since man people have to work on the same file, or are dependent on each others. We found that dividing the different exercises and not working at the same time on it lead to good results.

### Complexity of the Program
We assume that we have iteration_complexity, being the complexity of each iteration.
And storage_complexity, the complexity to store a value.
5.1) We assume this is with recompuatation at each time step. The number of calculations is <br/>
iteration_complexity*(n_loops*(n_loops+1)/2 * frequency)              if maxiter % frequency == 0  <br/>
iteration_complexity*(n_loops*(n_loops+1)/2 * frequency + maxiter)    otherwise  <br/>
with n_loops = floor(maxiter/frequency)

5.4) We calculate the complexity of the program wiht recompuatation at each time step (and the assumption that no inversion of the calculation is done). The number of evaluations is simply: <br/>
iteration_complexity*maxiter + storage_complexity*floor(maxiter/frequency)

5.5) Just by keeping the two member-variables, `current_index` and `current_value`, the minimum complexity for reverse calculation is the same as answer 5.1). On the other hand, there could be some smart summing of keeping the whole array of the values stored at each frequency etc. For the implementation of this, there needs to be knowledge of how fast the series descents and the datatype (precision) used to store it. <br/>

### Integral
6.4) The expected values are obtained at following iterations: <br/>
cube: it=101, int_sum=0.2549750025 <br/>
sin: it=16, int_sum=1.0483 <br/>
cos: it=63, int_sum=-0.049864 <br/>



