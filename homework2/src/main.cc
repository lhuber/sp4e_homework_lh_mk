// main.cc
#include <iostream>
#include <memory>
#include <stdio.h>
#include <string.h>

#include "series.cc"
#include "dumpers.cc"


int main(int argc, char ** argv) {
  // Parse Arguments 

  int maxiter = 100;
  int frequency = 1;

  std::string filename;
  bool dump_to_file = false;

  std::string separator = " ";

  std::shared_ptr<Series> MySeriesPointer;
  std::shared_ptr<DumperSeries> MyDumperPointer;

  for(int i = 1; i < argc; i++){
    std::string arg = argv[i];

    if(arg == "-n" && argc>(i+1)){
      // n for Series (arithmetic or pi)
      maxiter = std::stoi(argv[i+1]);

    }else if(arg == "-f" && argc>(i+1)){
      // f for Dumper output frequency
      frequency = std::stoi(argv[i+1]);

    }else if(arg == "-o" || arg == "--print"){
      // o for outputting in terminal as a text

    }else if(arg ==  "-w" ||  arg == "--write"){
      dump_to_file = true;
      // w for outputting in file using a delimiter
      if (argc>(i+1)) {
        std::string separator_type = argv[i+1];
        if (separator_type== "comma") {
          separator = ',';
        } else if(separator_type == "pipe")  {
          separator = '|';
        } // Default delimiter is comma
      }

    }else if(arg == "--arithmetic") {
      //a for computing arithmetic series
      MySeriesPointer.reset(new ComputeArithmetic());

    }else if(arg == "--pi") {
      MySeriesPointer.reset(new ComputePi());

    }else if(arg == "--cos" && argc>(i+2)) {
      MySeriesPointer.reset(new RiemannIntegral(std::stod(argv[i + 1]), std::stod(argv[i + 2]), cos_));

    }else if (arg == "--sin" && argc>(i+2)) {
      MySeriesPointer.reset(new RiemannIntegral(std::stod(argv[i + 1]), std::stod(argv[i + 2]), sin_));

    }else if (arg == "--cube" && argc>(i+2)) {
      MySeriesPointer.reset(new RiemannIntegral(std::stod(argv[i + 1]), std::stod(argv[i + 2]), cube));
    }
  }

    if(MySeriesPointer){
    std::cout << "Arg-parsing successfull.\n";
  }else{
    std::cout << "\nPlease provide meaningful arguments. \n\n" <<
    "If you wish to calculate series specify whether you wish to compute Pi (--pi) or Arithmetic (--arithmetic) \n\n" << 
    "If you want to calculate integral, then please specify (in order!): \n" <<
    "--cos, --sin, or --cube : function type, must be followed by two arguments below\n" <<
    "DOUBLE : interval start\n" <<
    "DOUBLE : interval end \n\n" <<

    "Shared parameters for series/integrals: \n" <<
    "-n INT : number of iterations/partitions \n" <<
    "-f INT : output frequency for Dumper class \n" <<
    "--write STRING : output to file \n" <<
    "--print : output to terminal \n" <<
    "-comma (or -pipe): separator type for the output \n\n";

	  // std::cout << "No useful series-argument obtained. Please specify whether you wish to compute Pi (--pi) or Arithmetic (--arithmetic). or --cos --sin --cube \nAborting...\n";
    std::cout << "Some examples of usage (in the build folder): " << "\n";
    std::cout << "./src/main [maxiter] [frequency] [print or write] [SeriesType] [delimter] [integral type] \n";
    std::cout << "./src/main --pi -n 1000 -f 10 --write comma" << " : Compute PI with 1000 iterations and output result to .csv file with frequency of 10" << "\n";
    std::cout << "./src/main --arithmetic -n 100 -f 1 --print" << " : Compute arithmetic series with 100 iterations and output result to the terminal file with frequency of 1" << "\n";
    std::cout << "./src/main -n 100 -f 1 --cube 0 1" << " : Compute integral_0^1(x^3dx) with 100 iterations" << "\n";
    std::cout << "./src/main --print -n 100 -f 1 --cos 0 3.1345" << " : Compute integral_0^3.1415(cos(x)dx) with 100 iterations" << "\n";
    std::cout << "./src/main --print -n 100 -f 1 --sin 0 1.5707" << " : Compute integral_0^3.1415(cos(x)dx) with 100 iterations" << "\n\n";
    return 0;
  }

  if(dump_to_file){
    MyDumperPointer.reset(new WriteSeries(maxiter, frequency, MySeriesPointer));
    MyDumperPointer->setSeparator(separator);
    std::ofstream my_stream;
    my_stream.open (MyDumperPointer->filename);
	
    std::cout << "Opening " << MyDumperPointer->filename << ".\n";
    my_stream << MyDumperPointer;
    std::cout << "Finished writting." << std::endl;
    my_stream.close();
	std::cout << "Closed file "<< MyDumperPointer->filename << "\n";
	
  } else {
    std::cout << "Start numerical series. \n";
    MyDumperPointer.reset(new PrintSeries(maxiter, frequency, MySeriesPointer));
    MyDumperPointer->setPrecision(5);
    std::ostream my_stream(std::cout.rdbuf());
    my_stream << MyDumperPointer;
    std::cout << "Series finished. \n";
  }
  return 0;
}
