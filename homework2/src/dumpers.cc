#include "dumpers.hh"


void DumperSeries::setPrecision(unsigned int precision){
  this->precision = precision;
};



PrintSeries::PrintSeries(const unsigned int maxiter, const unsigned int frequency,
                         std::shared_ptr <Series> series){
  this->maxiter = maxiter;
  this->frequency = frequency;
  this->series = series;
};

void PrintSeries::setSeparator(const std::string separator) {
  this->separator = separator;
}

void PrintSeries::dump(std::ostream &os) {
  double series_computed = this->series->compute(maxiter);
  double analytic_prediciton = this->series->getAnalyticPrediction(maxiter);
  double convergence = 0;
  double curent_val = 0;

  for (unsigned int ii = 0; ii < this->maxiter; ii += this->frequency) {
    curent_val = this->series->compute(ii);
    convergence = std::abs(curent_val - analytic_prediciton);
    os << "Iter: " << ii << "; Value: ";
    os << std::setprecision(this->precision) << curent_val << "; Convergence: ";
    if (!std::isnan(analytic_prediciton)) {
      os << std::setprecision(this->precision) << convergence << "\n";
    } else {
      os << "NaN\n";
    }
  };

  os << "Final iteration value: " << this->series->compute(this->maxiter);
  if (!std::isnan(analytic_prediciton))
    os << "; Analytic Prediction: " << std::setprecision(this->precision) << analytic_prediciton;

  convergence = std::abs(series_computed - analytic_prediciton);
  if (!std::isnan(analytic_prediciton))
    os << "; Convergence: " << std::setprecision(this->precision) << convergence;
  os << std::endl;
}



WriteSeries::WriteSeries(const unsigned int maxiter, const unsigned int frequency,
                         std::shared_ptr <Series> series) {
  this->maxiter = maxiter;
  this->frequency = frequency;
  this->series = series;

};

void WriteSeries::dump(std::ostream &os) {
  double curent_val = 0;

  for (int ii = 0; ii < this->maxiter; ii += frequency) {
    curent_val = this->series->compute(ii);
    os << std::setprecision(this->precision) << curent_val << separator;

  };
  
  os << this->series->compute(this->maxiter) << separator;
  os << std::setprecision(this->precision) << this->series->getAnalyticPrediction(maxiter);
};

void WriteSeries::setSeparator(const std::string separator) {
  this->separator = separator;

  if (separator == " ") {
    this->filename = "output.txt";

  } else if (separator == ",") {
    this->filename = "output.csv";

  } else if (separator == "|") {
    this->filename = "output.psv";

  } else {
    std::cout << "Default to 'output.txt'.";
    this->separator = " ";
    this->filename = "output.txt";
  }
};




inline std::ostream &operator<<(std::ostream &stream, std::shared_ptr <DumperSeries> _this) {
  _this->dump(stream);
  return stream;
};
