/* -------------------------------------------------------------------------- */
#pragma once

#include <iostream>
#include <cmath>
#include <math.h>
#include <functional>
/* -------------------------------------------------------------------------- */

enum FuncType { cube, sin_, cos_ };

class Series{
public:
  virtual double compute(unsigned int N = 1) = 0;
  virtual double getAnalyticPrediction(unsigned int N = 1){
    return std::nan("");
  }

protected:
  unsigned int current_index=0;
  double current_value=0.0;
};


class ComputeArithmetic: public Series {
public:
  double compute(unsigned int N = 1) override;
  double getAnalyticPrediction(unsigned int N = 1) override;
};



class ComputePi: public Series {
public:
  double compute(unsigned int N = 0) override;
  double getAnalyticPrediction(unsigned int N = 1) override;
};


class RiemannIntegral: public Series {
public:
  double a;
  double b;

  int func_type;
  std::function<double(double)> my_function;

  RiemannIntegral(double a, double b, int func_type);
  double compute(unsigned int N = 1) override;
  double getAnalyticPrediction(unsigned int N = 1) override;
};
