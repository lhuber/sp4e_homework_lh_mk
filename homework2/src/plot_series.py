#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 14:22:05 2021

@author: michael, lukas
"""
import os
import csv
import argparse

from enum import Enum, auto

import matplotlib.pyplot as plt

def main(filename: str):
    path = os.path.join("..", "build")

    filetype = filename[-3:]
    if filetype == "txt":
        delimiter = " "
    elif filetype == "psv":
        delimiter = "|"
    elif filetype == "csv":
        delimiter = ","
    else:
        raise Exception("Unexpected filetype.")
        
    try:
        with open(os.path.join(path, filename), newline='') as f:
            # For delimiter in possible_delimiters:
            csv_reader = csv.reader(f, delimiter=delimiter)

            value_list = []

            for row in csv_reader:
                value_list += row

    except FileNotFoundError:
        raise Exception(f"No file found at {os.path.join(path, filename)}")

    value_list = [float(value) for value in value_list]
    analytic_solution = value_list[-1]

    plt.title("Series visualisation")
    plt.xlabel("Steps")
    plt.ylabel("Value")
    plt.plot(value_list, color="blue")
    plt.plot([0, len(value_list)],
             [analytic_solution, analytic_solution], color="red")

    plt.xlim([0, len(value_list)])
    
    try:
        plt.yscale('log')
    except:
        pass

    # Stops when showing plot
    plt.show()


if (__name__) == "__main__":
    my_parser = argparse.ArgumentParser(description='Process input.')
    my_parser.add_argument('filename', metavar='filename', nargs="?",
                           help=("Default: output.csv"))
    
    args, unknown = my_parser.parse_known_args()

    if args.filename is not None:
        filename = args.filename
    else:
        raise Exception("No filename provided.")

    main(filename)
