/* -------------------------------------------------------------------------- */
#pragma once

#include <iostream>
#include <fstream>
#include <iomanip>

#include <cmath>
/* -------------------------------------------------------------------------- */

class DumperSeries{
protected:
  unsigned int maxiter;
  unsigned int frequency;
  unsigned int precision = 10;

  std::shared_ptr<Series> series;
  std::string separator = " ";

public:
  virtual void dump(std::ostream & os) = 0;

  void setPrecision(unsigned int precision);
  virtual void setSeparator(const std::string delimiter) = 0;
  std::string filename = "output.txt";
};

class PrintSeries: public DumperSeries{
public:
  PrintSeries(const unsigned int maxiter,
              const unsigned int frequency,
              std::shared_ptr<Series> series
  );

  void dump(std::ostream &os = std::cout) override;
  void setSeparator(const std::string delimiter) override;
};

class WriteSeries : public DumperSeries {
public:
  WriteSeries(const unsigned int maxiter,
              const unsigned int frequency,
              std::shared_ptr<Series> series
  );
  void dump(std::ostream &os = std::cout) override;
  void setSeparator(const std::string delimiter) override;
};
