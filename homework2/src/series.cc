#include "series.hh"

double ComputeArithmetic:: compute(unsigned int N){
  // Numerical sum-calculation
  int sum = 0;

  if (N>current_index)
    sum = current_value;
  else
    current_index = 0;
  // Starting at last value to avoid rounding erros
  for (int kk = current_index; kk <= N; kk++){
    sum += kk;
  }
  current_index = N;
  current_value = sum;
  return sum;
};

double ComputeArithmetic::getAnalyticPrediction(unsigned int N){
  return ((N + 1) * N / 2);
};




double ComputePi::compute(unsigned int N){
  // Numerical pi-computation
  double pi_calc = 0.0;
  double float_k;
  if (N>current_index)
    pi_calc = current_value;
  else
    current_index = 0;

  if(current_index==0)
    current_index = 1;

  // We don't do inverse calculation (since this was not explicetly asked)
  for (int kk = current_index; kk < N; kk++){
    float_k = kk;
    pi_calc += 1.0/(float_k*float_k);
  }
  current_value = pi_calc;
  pi_calc = pow(6*pi_calc, 0.5);
  current_index = N;
  return pi_calc;
};

double ComputePi::getAnalyticPrediction(unsigned int N){
  return M_PI;
};




RiemannIntegral::RiemannIntegral(double a, double b, int func_type) : a(a), b(b), func_type(func_type){
  switch (func_type) {
    case cube:
      this->my_function = [](double x) { return pow(x, 3); };
      break;
    case sin_:
      this->my_function = [](double x) { return sin(x); };
      break;
    case cos_:
      this->my_function = [](double x) { return cos(x); };
      break;
  }
}

double RiemannIntegral::compute(unsigned int N ){
  if (N <= 0){
    current_index = 1;
    current_value = 0.0;
    return 0;
  }
  current_value = 0;

  double dx = (this->b - this->a)/N;
  double x = 0;
  for (int kk = 1; kk <= N; kk++){
    x = this->a + kk * dx;
    current_value += my_function(x) * dx;
  }
  current_index = N;
  return current_value;
};

double RiemannIntegral::getAnalyticPrediction(unsigned int N) {
  double val = 0;
  switch (func_type) {
    case cube:
      return 1. / 4 * (pow(this->b, 4) - pow(this->a, 4));
      break;
    case sin_:
      return cos(this->a) - cos(this->b);
      break;
    case cos_:
      return sin(this->b) - sin(this->a);
      break;
  }
  return 0;
};
