#include "my_types.hh"
#include "system.hh"
#include "compute_temperature_finite_differences.hh"
#include "material_point.hh"
#include <gtest/gtest.h>
#include <complex>

/*****************************************************************/
TEST(temperature, homogeneous) {
  std::cout << "Starting it \n";
  UInt N = 3;

  Real uniform_temp = 1;
  System material_system;

  for (int ii = 0; ii < N; ii++){
    for (int jj = 0; jj < N; jj++){
      std::shared_ptr<MaterialPoint> newPoint( new MaterialPoint(
          uniform_temp, 0));

      material_system.addParticle(newPoint);
    }
  }

  auto myComputer = ComputeTemperatureFiniteDifferences(0.1);
  std::cout << "Got all assigned. \n";
//  myComputer.setDeltaT(1.0);
  // Evaluate several times
  for (int ii = 0; ii < 3; ii++)
    myComputer.compute(material_system);
  std::cout << "Done computing.. \n";
  for (auto &&entry : material_system) {
    auto material_particle = static_cast<MaterialPoint*>(&entry);

    ASSERT_NEAR(material_particle->getTemperature(),
                uniform_temp, 1e-10);
  }
}

/*****************************************************************/
TEST(temperature, steadystate) {
  UInt N = 50;

  Real x_min = -1.0;
  Real x_max = 1.;
  // Not used
//  Real y_min = -1.0;
//  Real y_max = 1.0;

  Real dx  = (x_max - x_min) / N;
  Real lengthX = x_max - x_min;
//  Real lengthY = y_max - y_min;

  // Create evolving and orignal system
  System material_system;
  System system_duplicate;

  for (int iy = 0; iy < N; iy++){
    for (int ix = 0; ix < N; ix++){
      Real x_val = dx*ix + x_min;
      Real my_heat_rate = std::pow(2*M_PI / lengthX, 2) * std::sin(2*M_PI * x_val/lengthX);
      Real my_temperature = std::sin(2*M_PI*x_val/lengthX);

      std::shared_ptr<MaterialPoint> newPoint(
          new MaterialPoint(my_temperature, my_heat_rate));
      material_system.addParticle(newPoint);

      // Do it again for comparison
      std::shared_ptr<MaterialPoint> duplicatePoint(
          new MaterialPoint(my_temperature, my_heat_rate));
      system_duplicate.addParticle(duplicatePoint);
    }
  }

  auto myComputer = ComputeTemperatureFiniteDifferences(1.0);
  myComputer.setL(lengthX);
//  myComputer.setDeltaT(1);
//  myComputer.setSystemLengthsXY(lengthX, lengthY);

  std::cout << "Starting computing. \n";
  // Evaluate several times
  for (int ii = 0; ii < 1; ii++)
    myComputer.compute(material_system);

  for (int ii = 0; ii < material_system.getNbParticles(); ii++) {
    auto material_particle = static_cast<MaterialPoint*>(&material_system.getParticle(ii));
    auto particle_duplicate = static_cast<MaterialPoint*>(&system_duplicate.getParticle(ii));

    ASSERT_NEAR(
        material_particle->getTemperature(),
        particle_duplicate->getTemperature(),
        1e-2);
  }
}
