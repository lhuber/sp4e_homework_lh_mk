#include "compute_temperature_finite_differences.hh"
#include "material_point.hh"
#include "matrix.hh"

void ComputeTemperatureFiniteDifferences::compute(System& system) {
  if (std::sqrt(system.getNbParticles()) < 3)
    throw std::invalid_argument( "Number of rows to few for derivative." );

  Eigen::SparseLU<Eigen::SparseMatrix<Real>> solver;
//  if (solverGridSize != system.getNbParticles()) {
  assembleLinearOperator(system);

  // Compute the ordering permutation vector from the structural pattern of A
  solver.analyzePattern(A);
  //  auto temp_A = Eigen::MatrixXd(A);
  for (int ii = 0; ii < system.getNbParticles(); ii++)
    A.coeffRef(ii, ii) += 1;
  // std::cout << Eigen::MatrixXd(A) << "\n";
  // Compute the numerical factorization
  solver.factorize(A);

  auto b = assembleRightHandSide(system);

  // Use the factors to solve the linear system
  Eigen::VectorXd new_temperature(system.getNbParticles());
  new_temperature = solver.solve(b);

  int ii = 0;
  for (auto & part : system)
    dynamic_cast<MaterialPoint&>(part).setTemperature(new_temperature(ii++));
}

void ComputeTemperatureFiniteDifferences::assembleLinearOperator(
    System& system) {
  UInt tot_size = system.getNbParticles();
  UInt cols = std::sqrt(tot_size);

  Real a = L / std::sqrt(system.getNbParticles());
  const auto factor = (-1)*delta_t * conductivity / density / capacity / a / a;

  // Reset matrix & refill the matrix
  A = Eigen::SparseMatrix<Real>(tot_size, tot_size);

  for (int ii = 0; ii < tot_size; ii++){
    A.insert(ii, ii) = (-4) * factor;

    if ((ii+1) % cols != 0)
      A.insert(ii, ii + 1) = factor;
    else
      A.coeffRef(ii, (ii - cols + 1 + tot_size) % tot_size) += factor;

    if (ii % cols > 0)
      A.insert(ii, ii - 1) = factor;
    else
      A.coeffRef(ii, (ii + cols - 1) % tot_size) += factor;

    if (ii >= cols)
      A.insert(ii, ii - cols) = factor;
    else
      A.coeffRef(ii, (ii - cols) + tot_size) += factor;

    if (ii < tot_size - cols)
      A.insert(ii, ii + cols) = factor;
    else
      A.coeffRef(ii, (ii + cols) - tot_size) += factor;
  }
}

Eigen::Matrix<Real, Eigen::Dynamic, 1>  ComputeTemperatureFiniteDifferences::assembleRightHandSide(
    System& system) {
  UInt tot_size = system.getNbParticles();

  const auto factor = delta_t/density/capacity;
//  Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic> rightHand(this->getL(),this->getL());
  Eigen::Matrix<Real, Eigen::Dynamic, 1> rightHand(tot_size);

  for (int ii = 0; ii < tot_size; ii++){
    auto& temp_particle = dynamic_cast<MaterialPoint&>(system.getParticle(ii));
    rightHand(ii) = factor*temp_particle.getHeatSource() + temp_particle.getTemperature();
  };
  return rightHand;
}
