#ifndef COMPUTE_TEMPERATURE_FINITE_DIFFERENCES_HH
#define COMPUTE_TEMPERATURE_FINITE_DIFFERENCES_HH

#include "compute.hh"
#include <Eigen/SparseCore>
#include <Eigen/SparseLU>
#include "matrix.hh"

class ComputeTemperatureFiniteDifferences : public Compute {
public:
  ComputeTemperatureFiniteDifferences(Real dt) : delta_t(dt) {
    solverGridSize = 0;
  }

  void compute(System& system) override;

//  Eigen::SparseMatrix<Real> assembleLinearOperator(System& system);
  void assembleLinearOperator(System& system);
  Eigen::Matrix<Real, Eigen::Dynamic, 1>  assembleRightHandSide(System& system);
//  void assembleRightHandSide(System& system);

  //! return the heat conductivity
  Real& getConductivity() { return conductivity; };
  //! return the heat capacity
  Real& getCapacity() { return capacity; };
  //! return the heat capacity
  Real& getDensity() { return density; };
  //! return the characteristic length of the square
  Real& getL() { return L; };
  //! set the characteristic length of the square
  void setL(Real newL) { L = newL; };
  //! return the characteristic length of the square
  Real& getDeltaT() { return delta_t; };

private:
  Real conductivity = 1;
  Real capacity = 1;
  Real density = 1;
  //! side length of the problem
  Real L = 1;

  //! Time step
  Real delta_t = 0.1;

  //! Storing solver and get an assigned check
//  Eigen::SparseLU<Eigen::SparseMatrix<Real>> solver1;
  Eigen::SparseMatrix<Real> A;
  //! Checkbox if the solver is already assigned [it is reset whenever the system size is changed]
  UInt solverGridSize;
};

#endif  // COMPUTE_TEMPERATURE_FINITE_DIFFERENCES_HH
