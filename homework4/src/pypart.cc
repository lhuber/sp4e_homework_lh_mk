#include <pybind11/pybind11.h>
#include <pybind11/functional.h>

namespace py = pybind11;

#include "compute_gravity.hh"
#include "compute_temperature.hh"
#include "compute_verlet_integration.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"


PYBIND11_MODULE(pypart, m) {
  m.doc() = "pybind of the Particles project 1.0";
  
  // bind the routines here
  py::class_<ParticlesFactoryInterface>(m, "ParticlesFactoryInterface")
    .def("getInstance", &ParticlesFactoryInterface::getInstance, py::return_value_policy::reference)
    .def("createSimulation",
     //     static_cast<SystemEvolution& (ParticlesFactoryInterface::*)(const std::string&, Real)>
          py::overload_cast<const std::string&, Real>
         (&ParticlesFactoryInterface::createSimulation),
         py::return_value_policy::reference,
         "Basic simulation setup.")
    .def("createSimulation",
     //     static_cast<SystemEvolution& (ParticlesFactoryInterface::*)(const std::string&, Real, std::function<void(ParticlesFactoryInterface&, Real)>)>
          py::overload_cast<const std::string&, Real, std::function<void(ParticlesFactoryInterface&, Real)>>
         (&ParticlesFactoryInterface::createSimulation),
         py::return_value_policy::reference,
         "Advanced setup.")
    .def_property_readonly("system_evolution", &ParticlesFactoryInterface::getSystemEvolution, py::return_value_policy::reference_internal)
    .def("__repr__",
       [](const ParticlesFactoryInterface &a)
         { return "Instance of <ParticlesFactoryInterface>"; });
  
  py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(m, "MaterialPointsFactory")
    .def("getInstance", &MaterialPointsFactory::getInstance, py::return_value_policy::reference);
    
  py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(m, "PingPongBallsFactory")
    .def("getInstance", &PingPongBallsFactory::getInstance, py::return_value_policy::reference);

  py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "PlanetsFactory")
    .def("getInstance", &PlanetsFactory::getInstance, py::return_value_policy::reference);
  
  py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute")
    .def("compute", &Compute::compute);
  
  py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature")
    .def(py::init<>())
    .def_property("conductivity", &ComputeTemperature::getConductivity,
                  [](ComputeTemperature &c, Real conductivity_) {c.getConductivity() = conductivity_;})
    .def_property("capacity", &ComputeTemperature::getCapacity,
                  [](ComputeTemperature &c, Real capacity_) {c.getCapacity() = capacity_;} )
    .def_property("density", &ComputeTemperature::getDensity,
                  [](ComputeTemperature &c, Real density_) {c.getDensity() = density_;} )
    .def_property("L", &ComputeTemperature::getL, 
                  [](ComputeTemperature &c, Real L_) {c.getL() = L_;})
    .def_property("deltat", &ComputeTemperature::getDeltat,
                  [](ComputeTemperature &c, Real deltat_) {c.getDeltat() = deltat_;});

  py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(m, "ComputeInteraction");

  py::class_<ComputeGravity, ComputeInteraction, std::shared_ptr<ComputeGravity>>(m, "ComputeGravity")
    .def(py::init<>())  
    .def("setG", &ComputeGravity::setG);

  py::class_<ComputeVerletIntegration, Compute, std::shared_ptr<ComputeVerletIntegration>>(m, "ComputeVerletIntegration")
    .def(py::init<Real>())  
    .def("addInteraction", &ComputeVerletIntegration::addInteraction);

  py::class_<SystemEvolution>(m, "SystemEvolution")
    .def("addCompute", &SystemEvolution::addCompute)
    .def("getSystem", &SystemEvolution::getSystem, py::return_value_policy::reference)
    .def("setNSteps", &SystemEvolution::setNSteps)
    .def("setDumpFreq", &SystemEvolution::setDumpFreq)
    .def("evolve", &SystemEvolution::evolve);

  py::class_<CsvWriter>(m, "CsvWriter")
    .def(py::init<const std::string&>())  
    .def("write", &CsvWriter::write);

  py::class_<System, std::shared_ptr<System>>(m, "System")
    .def(py::init<>());


}
