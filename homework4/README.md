# Homework 4
## Authors
Lukas Huber - lukas.huber@epfl.ch
Mikhail Koptev - mikhail.koptev@epfl.ch

## Comments Questions
### 3.2 
The `createComputes` function additionally takes a functor-argument, which updates the `system_evolution` of the `system` by adding a new `Compute` method, which differs for the `material_point` or `planets`. (It is not defined for `ping_pong`.

### 4.2
As `std::unique_ptr` can not be converted to `std::shared_ptr`, we have to change the holder on the registered types to `std::shared_ptr`. This had to be defined in the header of the class of the pybind.

## Setup and Build Project
``` bash
mkdir build && cd build && cmake ..
make
cd ..
```

## Build with Pybind

``` bash
cmake -DUSE_PYTHON=ON -DPYTHON_EXECUTABLE=$(which python) ..
make
cd ..
```

## Run Tests
Following tests scripts can be run from the homework4 folder:

``` bash
./build/src/test_fft
./build/src/test_kepler
./build/src/test_temperature
```
## Generate input temperature distribution
Call from homework4 folder:
``` bash
python src/generate_materialpoint_input.py 64 data/mp_input.csv
```

## (Optional) Execute CPP code
Call from homework4 folder:
``` bash
cd build/src
mkdir dumps
./particles 10 1 ../../data/mp_input.csv material_point 0.1
```

## Execute Python code bindings
Call from homework4 folder:
``` bash
cd build/src
mkdir dumps
python main.py 5 1 ../../data/mp_input.csv material_point 0.1
```
