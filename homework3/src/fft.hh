#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>

/* ------------------------------------------------------ */

struct FFT {
  static Matrix<complex> transform(Matrix<complex> &m);

  static Matrix<complex> itransform(Matrix<complex> &m);

  static Matrix<std::complex<int>> computeFrequencies(int size);
};

/* ------------------------------------------------------ */
inline Matrix<complex> FFT::transform(Matrix<complex> &m_in) {
  auto m_out = Matrix<complex>(m_in.size());

  auto p = fftw_plan_dft_2d(
      static_cast<int>(m_in.rows()),
      static_cast<int>(m_in.cols()),
      reinterpret_cast<fftw_complex *>(m_in.data()),
      reinterpret_cast<fftw_complex *>(m_out.data()),
      FFTW_FORWARD, FFTW_ESTIMATE);

  fftw_execute(p);
  fftw_destroy_plan(p);

  //    Divide to make fully reverse of the function
  // m_out /= (m_in.size() * m_in.size());

  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex> &m_in) {
  auto m_out = Matrix<complex>(m_in.size());

  auto p = fftw_plan_dft_2d(
      static_cast<int>(m_in.rows()),
      static_cast<int>(m_in.cols()),
      reinterpret_cast<fftw_complex *>(m_in.data()),
      reinterpret_cast<fftw_complex *>(m_out.data()),
      FFTW_BACKWARD, FFTW_ESTIMATE);

  fftw_execute(p);
  fftw_destroy_plan(p);
  //    Divide to make fully reverse of the function
  m_out /= (m_in.size() * m_in.size());


  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {
  // The real part of the frequency is in first direction
  // the imaginary in the second direction
  auto freqs = Matrix<std::complex<int>>(size);

  int delta_n;
  if (size % 2 == 0)
    delta_n = size / 2;
  else
    delta_n = (size - 1) / 2;

    for (auto &&entry: index(freqs)) {
      int i = std::get<0>(entry);
      int j = std::get<1>(entry);

      auto &val = std::get<2>(entry);
      val = std::complex<int>((i + delta_n) % size - delta_n,
                              (j + delta_n) % size - delta_n);
  }
  return freqs;
}

#endif  // FFT_HH
