#include "my_types.hh"
#include "system.hh"
#include "compute_temperature.hh"
#include "material_point.hh"
#include <gtest/gtest.h>
#include <complex>

/*****************************************************************/
TEST(temperature, homogeneous) {
  UInt N = 512;

  Real uniform_temp = 0;
  System material_system;

    for (int ii = 0; ii < N; ii++){
    for (int jj = 0; jj < N; jj++){
      std::shared_ptr<MaterialPoint> newPoint( new MaterialPoint());
      newPoint->setTemperature(uniform_temp);
      newPoint->setHeatRate(0);

      material_system.addParticle(newPoint);
    }
  }


  auto myComputer = ComputeTemperature();
  myComputer.setDeltaT(1.0);
  // Evaluate several times
  for (int ii = 0; ii < 3; ii++)
    myComputer.compute(material_system);

  for (auto &&entry : material_system) {
    auto material_particle = static_cast<MaterialPoint*>(&entry);

    ASSERT_NEAR(material_particle->getTemperature(),
                uniform_temp, 1e-10);
  }
}

/*****************************************************************/
TEST(temperature, steadystate) {
  UInt N = 512;

  Real x_min = -1.0;
  Real x_max = 1.;
  // Not used
  Real y_min = -1.0;
  Real y_max = 1.0;

  Real dx  = (x_max - x_min) / N;
  Real lengthX = x_max - x_min;
  Real lengthY = y_max - y_min;

  // Create evolving and orignal system
  System material_system;
  System system_duplicate;

  for (int iy = 0; iy < N; iy++){
    for (int ix = 0; ix < N; ix++){
      Real x_val = dx*ix + x_min;
      Real my_heat_rate = std::pow(2*M_PI / lengthX, 2) * std::sin(2*M_PI* x_val/lengthX);
      Real my_temperature = std::sin(2*M_PI*x_val/lengthX);

      std::shared_ptr<MaterialPoint> newPoint(
          new MaterialPoint(my_temperature, my_heat_rate));
      material_system.addParticle(newPoint);

      // Do it again for comparison
      std::shared_ptr<MaterialPoint> duplicatePoint(
          new MaterialPoint(my_temperature, my_heat_rate));
      system_duplicate.addParticle(duplicatePoint);
    }
  }

  auto myComputer = ComputeTemperature();
  myComputer.setDeltaT(1);
  myComputer.setSystemLengthsXY(lengthX, lengthY);

  std::cout << "Starting computing. \n";
  // Evaluate several times
  for (int ii = 0; ii < 1; ii++)
    myComputer.compute(material_system);

  for (int ii = 0; ii < material_system.getNbParticles(); ii++) {
    auto material_particle = static_cast<MaterialPoint*>(&material_system.getParticle(ii));
    auto particle_duplicate = static_cast<MaterialPoint*>(&system_duplicate.getParticle(ii));

    ASSERT_NEAR(
        material_particle->getTemperature(),
        particle_duplicate->getTemperature(),
        1e-10);
  }
}

/*****************************************************************/
TEST(temperature, steadystates_square) {
  UInt N = 512;

  Real x_min = -1.0;
  Real x_max = 1.0;

  Real y_min = -1;
  Real y_max = 1;

  Real dx  = (x_max - x_min) / N;
  Real xL = x_max - x_min;

  Real yL = y_max - y_min;

  Real peak_value = 2/(xL/N);

  System material_system;
  System system_duplicate;

  for (int iy = 0; iy < N; iy++){
    for (int ix = 0; ix < N; ix++){
      Real x_val = dx*ix + x_min;

      Real my_heat_rate;
      if(x_val == -0.5)
        my_heat_rate = (-1)*peak_value;
      else if (x_val == 0.5)
        my_heat_rate = peak_value;
      else
        my_heat_rate = 0.0;

      Real my_temperature;
      if (x_val <= -0.5)
        my_temperature = (-x_val) - 1;
      else if(x_val <= 0.5)
        my_temperature = x_val;
      else
        my_temperature = (-x_val) + 1;

      std::shared_ptr<MaterialPoint> newPoint(
          new MaterialPoint(my_temperature, my_heat_rate));
      material_system.addParticle(newPoint);

      // Do it again for comparison
      std::shared_ptr<MaterialPoint> duplicatePoint(
          new MaterialPoint(my_temperature, my_heat_rate));
      system_duplicate.addParticle(duplicatePoint);
    }
  }

  auto myComputer = ComputeTemperature();
  myComputer.setDeltaT(0.0001);
  myComputer.setSystemLengthsXY(xL, yL);

  std::cout << "Starting computing. \n";
  // Evaluate multiple times
  for (int ii = 0; ii < 1; ii++)
    myComputer.compute(material_system);

  for (int ii = 0; ii < material_system.getNbParticles(); ii++) {
    auto material_particle = static_cast<MaterialPoint*>(&material_system.getParticle(ii));
    auto particle_duplicate = static_cast<MaterialPoint*>(&system_duplicate.getParticle(ii));

  ASSERT_NEAR(
      material_particle->getTemperature(),
      particle_duplicate->getTemperature(),
    1e-1);

  }
}
