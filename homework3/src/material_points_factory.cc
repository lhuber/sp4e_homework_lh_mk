#include "material_points_factory.hh"
#include "compute_temperature.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "material_point.hh"
#include <cmath>
#include <iostream>
/* -------------------------------------------------------------------------- */

std::unique_ptr<Particle> MaterialPointsFactory::createParticle() {
  return std::make_unique<MaterialPoint>();
}

/* -------------------------------------------------------------------------- */

SystemEvolution&
MaterialPointsFactory::createSimulation(const std::string& fname,
                                        Real timestep) {

  this->system_evolution =
      std::make_unique<SystemEvolution>(std::make_unique<System>());

  CsvReader reader(fname);
  reader.read(this->system_evolution->getSystem());

  // check if it is a square number
  auto N = this->system_evolution->getSystem().getNbParticles();
  int side = std::sqrt(N);
  if (side * side != N)
    throw std::runtime_error("number of particles is not square");

  auto temperature = std::make_shared<ComputeTemperature>();
  temperature->setDeltaT(timestep);

  if (fname.find("zeroboundary") != std::string::npos) {
      temperature->setBoundaryZero(true);
    }

  
  this->system_evolution->addCompute(temperature);
  
  //determine particle range on x and y axis
  auto xmin = this->system_evolution->getSystem().getParticle(0).getPosition()[0];
  auto xmax = this->system_evolution->getSystem().getParticle(0).getPosition()[0];
  auto ymin = this->system_evolution->getSystem().getParticle(0).getPosition()[1];
  auto ymax = this->system_evolution->getSystem().getParticle(0).getPosition()[1];

  for (uint i = 0; i< N; i++){
    auto xcur = this->system_evolution->getSystem().getParticle(i).getPosition()[0];
    auto ycur = this->system_evolution->getSystem().getParticle(i).getPosition()[1];
    if(xcur < xmin) xmin = xcur;
    if(xcur > xmax) xmax = xcur;
    if(ycur < ymin) ymin = ycur;
    if(ycur > ymax) ymax = ycur;
  }
  std::cout<<"System domain: [" << xmin << ", "<<xmax<<"]x[" << ymin << ", " <<ymax <<"] "<<std::endl;
  temperature->setSystemLengthsXY(xmax-xmin, ymax-ymin);
  return *system_evolution;
}

/* -------------------------------------------------------------------------- */

ParticlesFactoryInterface& MaterialPointsFactory::getInstance() {
  if (not ParticlesFactoryInterface::factory)
    ParticlesFactoryInterface::factory = new MaterialPointsFactory;

  return *factory;
}

/* -------------------------------------------------------------------------- */
