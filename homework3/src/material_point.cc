#include "material_point.hh"

/* -------------------------------------------------------------------------- */
void MaterialPoint::printself(std::ostream& stream) const {
  Particle::printself(stream);
  // Two space to differentiate temperature from heat-rate
  stream << "  " << temperature << " " << heat_rate;
  // if(typeid(stream) == typeid(std::cout))
  //   std::cout <<"MATERIAL POINT OUT!" <<std::endl;
}

/* -------------------------------------------------------------------------- */

void MaterialPoint::initself(std::istream& sstr) {
  Particle::initself(sstr);
  sstr >> temperature >> heat_rate;
}

void MaterialPoint::setTemperature(Real temperature){
  this->temperature = temperature; }

void MaterialPoint::setHeatRate(Real heat_rate){
  this->heat_rate = heat_rate;
}