#ifndef __MATERIAL_POINT__HH__
#define __MATERIAL_POINT__HH__

/* -------------------------------------------------------------------------- */
#include "particle.hh"

//! Class for MaterialPoint
class MaterialPoint : public Particle {
  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

public:
  //! Material Point initializer which takes temperature and heat_rate
  MaterialPoint() {};
  MaterialPoint(Real temperature, Real heat_rate): temperature(temperature), heat_rate(heat_rate) {};

  void printself(std::ostream& stream) const override;
  void initself(std::istream& sstr) override;

  //! Get temperature
  Real & getTemperature(){return temperature;};
  //! Get heat rate
  Real & getHeatRate(){return heat_rate;};

  //! Set temperature
  void setTemperature(Real temperature);
  //! Set heat rate
  void setHeatRate(Real heat_rate);

private:
  Real temperature;
  Real heat_rate;
};

/* -------------------------------------------------------------------------- */
#endif  //__MATERIAL_POINT__HH__
