#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>
#include <complex>

/*****************************************************************/
TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto &&entry: index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto &val = std::get<2>(entry);
    val = cos(k * i);
//    if (i == 0 && j == 0)
//      std::cout << i << "," << j << " = " << val << std::endl;
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto &&entry: index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto &val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, inverse_transform) {
  UInt N = 512;
  Matrix<complex> res(N);

  for (auto &&entry: index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto &val = std::get<2>(entry);

    if (i == 1 && j == 0)
      val = complex(N * N / 2, 0);
    else if (i == N - 1 && j == 0)
      val = complex(N * N / 2, 0);
    else
      val = complex(0, 0);

    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;
  }

  Matrix<complex> m_inv = FFT::itransform(res);

  Real k = 2 * M_PI / N;
  for (auto &&entry: index(m_inv)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto &val = std::get<2>(entry);

    ASSERT_NEAR(std::abs(val), std::abs(cos(k * i)), 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, compute_requencies) {
  UInt N_odd = 11;
  int vals_odd[] = {0, 1, 2, 3, 4, 5, -5, -4, -3, -2, -1};

  Matrix<std::complex<int>> freqs_odd = FFT::computeFrequencies(N_odd);

  for (auto &&entry: index(freqs_odd)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto &val = std::get<2>(entry);

    ASSERT_NEAR(std::abs(val),
                std::abs(std::complex<int>(vals_odd[i], vals_odd[j])),
                1e-10);
  }

  UInt N_even = 10;
  int vals_even[] = {0, 1, 2, 3, 4, -5, -4, -3, -2, -1};

  Matrix<std::complex<int>> freq_even = FFT::computeFrequencies(N_even);

  for (auto &&entry: index(freq_even)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto &val = std::get<2>(entry);

    ASSERT_NEAR(std::abs(val),
                std::abs(std::complex<int>(vals_even[i], vals_even[j])),
                1e-10);
  }
}
/*****************************************************************/
