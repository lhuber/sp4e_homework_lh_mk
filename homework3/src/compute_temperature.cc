#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */

inline UInt getArrayIndexFromMatrixIndeces(UInt i, UInt j, UInt size){
  UInt index =i + j*size;
  return index;
};

void ComputeTemperature::setDeltaT(Real dt) { this->dt = dt; }

void ComputeTemperature::compute(System& system) {
  // FFT
  UInt size = std::sqrt(system.getNbParticles());
  Matrix<complex> m_temperature(size);
  Matrix<complex> m_heatRate(size);

  for (auto &&entry: index(m_temperature)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);

    auto temp_index = getArrayIndexFromMatrixIndeces(i, j, size);
    MaterialPoint* temperature_point = static_cast<MaterialPoint*>(&system.getParticle(temp_index));

    auto &val = m_temperature(i, j);
    val = complex(temperature_point -> getTemperature(), 0);

    auto &valHeat = m_heatRate(i, j);
    valHeat = complex(temperature_point -> getHeatRate(), 0);
  }

  auto frequencies = FFT::computeFrequencies(size);
  Matrix<complex> mf_temperature = FFT::transform(m_temperature);
  Matrix<complex> mf_heatRate = FFT::transform(m_heatRate);

  for (auto &&entry: index(mf_temperature)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto &val = std::get<2>(entry);

    Real q_x = 2*M_PI/this->systemLengthX * frequencies(i, j).real();
    Real q_y = 2*M_PI/this->systemLengthY * frequencies(i, j).imag();

   val = 1./(massDensity * heatCapacity)*(
        mf_heatRate(i, j) - heatConductivity*val*(q_x*q_x + q_y*q_y));

  }

  Matrix<complex> m_d_temperature = FFT::itransform(mf_temperature);

  for (auto &&entry: index(m_temperature)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);

    auto temp_index = getArrayIndexFromMatrixIndeces(i, j, size);

    auto &val = m_temperature(i, j);
    MaterialPoint* temperature_point = static_cast<MaterialPoint*>(
        &system.getParticle(temp_index));

    temperature_point->setTemperature(
        temperature_point->getTemperature()
        + this->dt * m_d_temperature(i, j).real()
        );

    //setting boundary condition T=0 for points on edges
    if(this->BOUNDARY_ZERO)
      if(i*j==0 or i==size or j==size)
        temperature_point->setTemperature(0);
  }
}

/* -------------------------------------------------------------------------- */