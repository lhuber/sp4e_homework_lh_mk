#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

inline UInt getArrayIndexFromMatrixIndeces(UInt i, UInt j, UInt size);

//! Compute contact interaction between ping-pong balls
class ComputeTemperature : public Compute {
public:
  //! Penalty contact implementation
  void compute(System& system) override;
  //! Set time step
  void setDeltaT(Real dt);
  //! Set boundary condition T=0
  void setBoundaryZero(bool BOUNDARY_ZERO) { this->BOUNDARY_ZERO = BOUNDARY_ZERO; }
  //! Set mass density
  void setMassDensity(Real massDensity){
    this->massDensity = massDensity;
  }
  //! Set head capacity
  void setHeatCapacity(Real heatCapacity){
    this->heatCapacity = heatCapacity;
  }
  //! Set heat conductivity
  void setHeatConductivity(Real heatConductivity){
    this->heatConductivity = heatConductivity;
  }
  //! Set time mass system length in x and y direciton
  void setSystemLengthsXY(Real xL, Real yL){
    this->systemLengthX = xL;
    this->systemLengthY = yL;
  }
  

private:
  Real massDensity = 1;
  Real heatCapacity= 1;
  Real heatConductivity = 1;
  bool BOUNDARY_ZERO = 0;
  Real systemLengthX=1;
  Real systemLengthY=1;

  Real dt;
};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
