#include "particle.hh"

void Particle::printself(std::ostream& stream) const {
  stream << " " << position;
  stream << " " << velocity;
  stream << " " << force;
  stream << " " << mass;
  // if(typeid(stream) == typeid(std::cout))
  //   std::cout <<"PARTICLE OUT!" <<std::endl;
}

/* -------------------------------------------------------------------------- */

void Particle::initself(std::istream& sstr) {
  sstr >> position;
  sstr >> velocity;
  sstr >> force;
  sstr >> mass;
}
