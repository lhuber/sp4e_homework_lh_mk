import numpy as np

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("side", help="number of particles on the side", type=int)
parser.add_argument("filename", help="name of generated input file")
parser.add_argument("range", help="system square domain range", type=float)
parser.add_argument("radius", help="Radius for x^2+y^2<R", type=float)

args = parser.parse_args()

#assert square shape
side = int(args.side)
number = side*side
    
#domain side length, and lowerleft corner
L = args.range
xmin = -L/2
ymin = -L/2
xmax = xmin+L
ymax = ymin+L

#initialize arrays
# coords = np.zeros((number, 3))
velocities = np.zeros((number, 3))
forces = np.zeros((number, 3))
masses = np.ones((number, 1))
temps = np.zeros((number, 1))
heat_rates = np.zeros((number, 1))

#populate arrays
k=0
dx = L/(side-1)
dy = L/(side-1)
coords = []
for i in range(side):
    for j in range(side):
        x_coord = xmin+i*dx
        y_coord = ymin+j*dy
        coords.append([x_coord, y_coord, 0])
        if x_coord*x_coord+y_coord*y_coord<args.radius:    
            heat_rates[k]=1
        # print(x_coord, y_coord, heat_rates[k])
        k+=1
coords_num = np.array(coords)
file_data = np.hstack((coords_num, velocities, forces, masses, temps, heat_rates))
np.savetxt(args.filename, file_data, delimiter=" ")









