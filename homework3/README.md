# Homework 3
## Authors
Lukas Huber - lukas.huber@epfl.ch
Mikhail Koptev - mikhail.koptev@epfl.ch

## Demonstration
This repository describes the process which allows to create heat simulation as seen in the image below.
![Heating Demo](data/demo.gif)

## Requirements
This library is dependent on the FFTW library. Install by using:

``` bash
sudo apt install libfftw3-dev
```

## Documentation
The documentation can be build using doxygen, see 
https://www.doxygen.nl/index.html

Note that it will be put in a `docs` folder
``` bash
cd src
doxygen Doxfile
cd ..
```

Open it in your browser (here firefox):
``` bash
firefox docs/html/index.html
```

## File Structure
### Comments with Respect to the New Classes
`MaterialPoint` is a child class of `Particle`. </br>
`MaterialPointFactory` is a child class of `ParticlesFactoryInterface`. </br>
</br>
`Matrix` is a class template of type `T` -> this can probably be replaced by a particle.
It has member-function which is of type `T*`, where all the data is stored. </br>
</br>
`FFT` has a `transform` and `intransform` function to execute fourrier and inverse fourrier transforms.

### General Organization of Particles in the Various Objects
In general, function and classes are function templates and class templates respectively with respect to type `T`. This type is then assigned to be a specific particle, hence makes the code generic (easily adaptable to different code-types).


## Setup and Build Project
``` bash
mkdir build && cd build
cmake ..
make
cd ..
```
## Run Tests
Following tests scripts can be run from the homework3 folder:

``` bash
./build/src/test_fft
./build/src/test_kepler
./build/src/test_temperature
```
### How to repeat result shown in the header
## Create initial heat distribution with python script
The data script takes three arguments, and can be run as follows:
``` bash
python scripts/generate_circular_input.py [number of points on the side] [filename] [square side length] [radius of heat source]
```

For example:
``` bash
python3 scripts/generate_circular_input.py 256 data/input_256_zeroboundary.csv 4 0.1
```

## Run timesteps
The simulation can then be run in following manner:
``` bash
./build/src/particles 100 1 data/input_256_zeroboundary.csv material_point 0.00003

```

## Finally the Simulation results can be imported to Paraview:
1. Run Paraview
2. Click File->Open..., then select batch of step-0000X from "data/dumps/" folder, import as CSV (should be autodected)
3. In pipeline browser properties of imported files, change delimeter to " ", tick "Merge Consecutive Delimiters", untick "Have Headers", click Apply
4. Add filter "Table to points", select Fields 0,1,2 as X,Y,Z coordinates respectively, tick 2D Points, click Apply
5. Under Display (GeometryRepresentation) change Coloring to Field 10, and Representation to Points. Adjust Point size according to your preference.
6. Now you can run the simulation.

## Comment on the boundary condition T=0
To minimize code changes to the base classes we decided to pass boolean flag of boundary condition along with the inputs filename. If name of the input .csv file contains "zeroboundary" substring, then temperature of points on the boundary will be forcefully zeroed on every iteration. 
